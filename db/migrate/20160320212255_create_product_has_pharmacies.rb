class CreateProductHasPharmacies < ActiveRecord::Migration
  def change
    create_table :product_has_pharmacies do |t|
      t.references :pharmacy, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
