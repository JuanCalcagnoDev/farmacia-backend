class CreateFundations < ActiveRecord::Migration
  def change
    create_table :fundations do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
