class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.references :user, index: true, foreign_key: true
      t.references :fundation, index: true, foreign_key: true
      t.integer :Quantity
      t.string :product
      t.timestamps null: false
    end
  end
end
