class CreatePharmacies < ActiveRecord::Migration
  def change
    create_table :pharmacies do |t|
      t.string :name
      t.string :address
      t.references :city, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
