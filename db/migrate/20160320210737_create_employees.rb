class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :employeeName
      t.string :employeeLastname
      t.string :employeeAddress
      t.string :employeePhone
      t.string :employeePhone
      t.datetime :employeeBirthdate
      t.string :employeeNotes
      t.references :pharmacy, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
