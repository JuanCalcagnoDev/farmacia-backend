class CreatePharmacyHasUsers < ActiveRecord::Migration
  def change
    create_table :pharmacy_has_users do |t|
      t.references :user, index: true, foreign_key: true
      t.references :pharmacy, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
