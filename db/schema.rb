# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160326231150) do

  create_table "bill_details", force: :cascade do |t|
    t.integer  "quantity",   limit: 4
    t.integer  "totalPrice", limit: 4
    t.integer  "product_id", limit: 4
    t.integer  "bill_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "bill_details", ["bill_id"], name: "index_bill_details_on_bill_id", using: :btree
  add_index "bill_details", ["product_id"], name: "index_bill_details_on_product_id", using: :btree

  create_table "bills", force: :cascade do |t|
    t.string   "date",          limit: 255
    t.integer  "controlNumber", limit: 4
    t.integer  "subtotal",      limit: 4
    t.integer  "tax",           limit: 4
    t.integer  "total",         limit: 4
    t.integer  "user_id",       limit: 4
    t.integer  "pharmacy_id",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "bills", ["pharmacy_id"], name: "index_bills_on_pharmacy_id", using: :btree
  add_index "bills", ["user_id"], name: "index_bills_on_user_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "categoryName", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "cityName",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "donations", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.integer  "fundation_id", limit: 4
    t.integer  "Quantity",     limit: 4
    t.string   "product",      limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "donations", ["fundation_id"], name: "index_donations_on_fundation_id", using: :btree
  add_index "donations", ["user_id"], name: "index_donations_on_user_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.string   "employeeName",      limit: 255
    t.string   "employeeLastname",  limit: 255
    t.string   "employeeAddress",   limit: 255
    t.string   "employeePhone",     limit: 255
    t.datetime "employeeBirthdate"
    t.string   "employeeNotes",     limit: 255
    t.integer  "pharmacy_id",       limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "employees", ["pharmacy_id"], name: "index_employees_on_pharmacy_id", using: :btree

  create_table "fundations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "pharmacies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "address",    limit: 255
    t.integer  "city_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "pharmacies", ["city_id"], name: "index_pharmacies_on_city_id", using: :btree

  create_table "pharmacy_has_users", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "pharmacy_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "pharmacy_has_users", ["pharmacy_id"], name: "index_pharmacy_has_users_on_pharmacy_id", using: :btree
  add_index "pharmacy_has_users", ["user_id"], name: "index_pharmacy_has_users_on_user_id", using: :btree

  create_table "product_has_pharmacies", force: :cascade do |t|
    t.integer  "pharmacy_id", limit: 4
    t.integer  "product_id",  limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "product_has_pharmacies", ["pharmacy_id"], name: "index_product_has_pharmacies_on_pharmacy_id", using: :btree
  add_index "product_has_pharmacies", ["product_id"], name: "index_product_has_pharmacies_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "price",       limit: 4
    t.integer  "stock",       limit: 4
    t.integer  "category_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "lastname",   limit: 255
    t.string   "address",    limit: 255
    t.string   "phone",      limit: 255
    t.string   "email",      limit: 255
    t.string   "token",      limit: 255
    t.string   "password",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "bill_details", "bills"
  add_foreign_key "bill_details", "products"
  add_foreign_key "bills", "pharmacies"
  add_foreign_key "bills", "users"
  add_foreign_key "donations", "fundations"
  add_foreign_key "donations", "users"
  add_foreign_key "employees", "pharmacies"
  add_foreign_key "pharmacies", "cities"
  add_foreign_key "pharmacy_has_users", "pharmacies"
  add_foreign_key "pharmacy_has_users", "users"
  add_foreign_key "product_has_pharmacies", "pharmacies"
  add_foreign_key "product_has_pharmacies", "products"
  add_foreign_key "products", "categories"
end
