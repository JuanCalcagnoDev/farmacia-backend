# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
nacho = User.create(name:'Nacho')
caracas = City.create(cityName:'Caracas')

locatel    = Pharmacy.create(name: 'Locatel'   ,address:'La trinidad',city:caracas)
farmatodo  = Pharmacy.create(name: 'Farmatodo' ,address:'La union',   city:caracas)
farmahorro = Pharmacy.create(name: 'Farmahorro',address:'La boeyera', city:caracas)
farmarket  = Pharmacy.create(name: 'Farmarket' ,address:'El Hatillo', city:caracas)

analgesicos    = Category.create(categoryName:"Analgesicos")
antibioticos   = Category.create(categoryName:"Antibioticos")
antialergicos  = Category.create(categoryName:"Antialergicos")
Antiaconceptivos  = Category.create(categoryName:"Anticonceptivos")

loratadina  = Product.create(name:'Loratadina',   price:500,  stock:20, category:antialergicos)
benadryl    = Product.create(name:'benadryl',   price:800,  stock:20, category:antialergicos)
quadriderm  = Product.create(name:'quadriderm',   price:300,  stock:20, category:antialergicos)
alergitrat  = Product.create(name:'alergitrat',   price:200,  stock:20, category:antialergicos)

atamel      = Product.create(name:'Atamel',       price:670, stock:20, category:analgesicos)
ibuprofeno  = Product.create(name:'Ibuprofeno',       price:1100, stock:20, category:analgesicos)
aspirina    = Product.create(name:'Aspirina',       price:200, stock:20, category:analgesicos)
tiocolfen   = Product.create(name:'Tiocolfen',       price:1300, stock:20, category:analgesicos)


amoxicilina = Product.create(name:'Amoxicilina',  price:2000, stock:20, category:antibioticos)
penicilina  = Product.create(name:'Penicilina',  price:2000, stock:20, category:antibioticos)
ampicilina  = Product.create(name:'Ampicilina',  price:2000, stock:20, category:antibioticos)


ProductHasPharmacy.create(pharmacy:locatel,     product:atamel)
ProductHasPharmacy.create(pharmacy:locatel,     product:ibuprofeno)
ProductHasPharmacy.create(pharmacy:locatel,     product:penicilina)
ProductHasPharmacy.create(pharmacy:locatel,     product:benadryl)


ProductHasPharmacy.create(pharmacy:farmatodo,   product:loratadina)
ProductHasPharmacy.create(pharmacy:farmatodo,   product:aspirina)
ProductHasPharmacy.create(pharmacy:farmatodo,   product:quadriderm)
ProductHasPharmacy.create(pharmacy:farmatodo,   product:ampicilina)

ProductHasPharmacy.create(pharmacy:farmahorro,  product:amoxicilina)
ProductHasPharmacy.create(pharmacy:farmahorro,  product:penicilina)
ProductHasPharmacy.create(pharmacy:farmahorro,  product:loratadina)
ProductHasPharmacy.create(pharmacy:farmahorro,  product:atamel)
ProductHasPharmacy.create(pharmacy:farmahorro,  product:tiocolfen)

Fundation.create(name:'FundaProcura')
Fundation.create(name:'Hogar Bambi')
Fundation.create(name:'Asociación Ayuda a un Niño')
