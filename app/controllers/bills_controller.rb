class BillsController < ApplicationController  
  
  before_action :authenticate
  
  def index
    bills = Bill.all

    if bills
      render json: bills
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    bill = Bill.find(params[:id])

    if bill
      render json: bill
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create 

    bill = Bill.create(permit_params)

    if bill 
      render json: bill
    else
      render json:{message:'IncorrectParams'}
    end

  end


  def update

  end

  def access   
 
  end


  private

  def permit_params
    params.permit(:id,:user_id , :pharmacy_id, bill_details_attributes:[:id,:product_id,:bill_id,:quantity])
  end

end