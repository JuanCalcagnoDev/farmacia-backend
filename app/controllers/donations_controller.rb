class DonationsController < ApplicationController  
  
  before_action :authenticate
  
  def index
    donations = Donation.all

    if donations
      render json: donations
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    donation = Donation.find(params[:id])

    if donation
      render json: donation
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create 

    donation = Donation.create(permit_params)

    if donation 
      render json: donation
    else
      render json:{message:'IncorrectParams'}
    end

  end

  def user_donations
    
    donations = Donation.where(user_id: params[:user_id])

    if donations
      render json: donations
    else
      render json:{message: 'MAMALO'}
    end
    
  end

  def update

  end

  def access   
 
  end


  private

  def permit_params
    params.permit(:user_id, :fundation_id, :Quantity, :product)
  end

end