class ProductHasPharmaciesController < ApplicationController  

  before_action :authenticate

  def index
     @pharmacies = ProductHasPharmacy.where(product_id: params[:id])

    if @pharmacies
      render json: @pharmacies.to_json( :include => [:pharmacy] )
      # @pharmacies
    else
      render json:{message: 'MAMALO'}
    end

  end

  def products_by_pharmacy
    
    @products = ProductHasPharmacy.where(pharmacy_id: params[:id])

    if @products
      render json: @products.to_json( :include => [:product] )
      # @pharmacies
    else
      render json:{message: 'MAMALO'}
    end
  end

  def show


  end

  def create 
    
  end


  def update

  end


  private

  def permit_params
  end

end