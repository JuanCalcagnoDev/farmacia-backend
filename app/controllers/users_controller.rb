class UsersController < ApplicationController  
  
  before_action :authenticate, except: [ :create, :access ]

  def index
    users = User.all
    if users
      render json: users
    else
      record_not_found( "Failed Resource" )
    end
  end

  def create 
    
    user = User.create(user_params)

    if user.save
      render json: user
    else
      render json:{message: 'Incorrect Params'}
    end

  end

  def show
 
    user = User.find(params[:id])

    if user
      render json: user
    else
      render json:{message: '404 - Not found'}
    end

  end

  def update

  end


  def access   
    user = User.find_by_email( params[ :email ] )
   # puts params[:user][0] == ''
   
    if params[ :email ] && params[:password]
      if user && user.password == params[ :password ]
        payload    = { data: user.email }
        user.token = JWT.encode( payload , nil , 'none' ) 
        render json: user
      else
        record_not_found( "Access Error" )
      end    
 
    else
      record_not_found( "Access Error" )
    end

  end

  private

  def user_params
    params.require(:user).permit( :name , :lastname , :address, :phone , :email , :password )
  end

end