class ProductsController < ApplicationController  

 before_action :authenticate 
 
  def index
    products = Product.where(category_id: params[:category_id])

    if products
      render json: products
    else
      render json:{message: 'MAMALO'}
    end

  end  

  def all_products
    products = Product.all

    if products
      render json: products
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    product = Product.find(params[:id])

    if product
      render json: product
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create 


  end


  def update

  end

  def access   
 
  end


  private

  def user_params
  end

end