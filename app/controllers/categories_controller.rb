class CategoriesController < ApplicationController  

    before_action :authenticate

  def index
    categories = Category.all

    if categories
      render json: categories
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    category = Category.find(params[:id])

    if category
      render json: category
    else
      render json:{message: '404 - Not found'}
    end

  end

  private

  def permit_params
  end

end