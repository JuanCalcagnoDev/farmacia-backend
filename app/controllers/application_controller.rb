class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :null_session

    def authenticate
    if request.headers[ 'X-Token' ].present?
      token        = request.headers[ "X-Token" ]
      payload      = JWT.decode( token , nil , false )
      @logged_user = User.find_by( email: payload.first[ 'data' ] )
    else
      record_not_found( "Authentication Error" )
    end
  end 
  
end
