class PharmaciesController < ApplicationController  

  before_action :authenticate 
  
  def index
    pharmacies = Pharmacy.all

    if pharmacies
      render json: pharmacies
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    pharmacies = Pharmacy.find(params[:id])

    if pharmacies
      render json: pharmacies
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create 


  end


  def update

  end

  def access   
 
  end


  private

  def user_params
  end

end