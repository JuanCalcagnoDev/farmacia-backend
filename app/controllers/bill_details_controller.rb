class BillDetailsController < ApplicationController  

  before_action :authenticate

  def index
    billsDetails = BillDetail.where(bill_id: params[:bill_id])

    if billsDetails
      render json: billsDetails
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    billsDetail = BillDetail.find(params[:id])

    if billsDetail
      render json: billsDetail
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create 
    
  end


  def update

  end

  def access   
 
  end


  private

  def permit_params
  end

end