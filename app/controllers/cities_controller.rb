class CitiesController < ApplicationController  

    before_action :authenticate

  def index
    cities = City.all

    if cities
      render json: cities
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    city = City.find(params[:id])

    if city
      render json: city
    else
      render json:{message: '404 - Not found'}
    end

  end

  private

  def permit_params
  end

end