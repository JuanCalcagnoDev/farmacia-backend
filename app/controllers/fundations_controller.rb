class FundationsController < ApplicationController  
  
  before_action :authenticate
  
  def index
    fundations = Fundation.all

    if fundations
      render json: fundations
    else
      render json:{message: 'MAMALO'}
    end

  end

  def show

    fundation = Fundation.find(params[:id])

    if fundation
      render json: fundation
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create 


  end


  def update

  end

  def access   
 
  end


  private

  def permit_params
  end

end