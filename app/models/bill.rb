class Bill < ActiveRecord::Base
  belongs_to :user
  belongs_to :pharmacy
  has_many :bill_details
  
  before_save   :saveUserInfo
  before_create :defaul_numbers

  accepts_nested_attributes_for :bill_details

  def defaul_numbers
    self.subtotal  = 0
    self.tax = 0
    self. total = 0  
  end


  def saveUserInfo
    self.date = self.created_at  
  end

end
