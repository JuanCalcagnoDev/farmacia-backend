class PharmacyHasUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :pharmacy
end
