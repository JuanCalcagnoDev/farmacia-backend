class Pharmacy < ActiveRecord::Base
  belongs_to :city
  belongs_to :product
  belongs_to :user

  has_many :employees
  has_many :product_has_pharmacies
  has_many :product, through: :product_has_pharmacies
  has_many :pharmacy_has_user
  has_many :users, through: :pharmacy_has_user
  
end
