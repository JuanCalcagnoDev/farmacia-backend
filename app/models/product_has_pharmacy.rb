class ProductHasPharmacy < ActiveRecord::Base
  belongs_to :pharmacy
  belongs_to :product
end
