class Product < ActiveRecord::Base
  
  belongs_to :category
  
  has_many :bill_details
  has_many :bills, through: :bill_details
  has_many :product_has_pharmacies
  has_many :pharmacies, through: :product_has_pharmacies
  
end
