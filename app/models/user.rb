class User < ActiveRecord::Base

	has_many :pharmacies 
	has_many :bills
	has_many :bill_details, through: :bills
  has_many :donations
  has_many :fundations, through: :donations

  validates :email                , presence: true
  validates :password             , presence: true


end
