Rails.application.routes.draw do
  
  resources :users      , only: [ :index , :create , :show , :update , :destroy ] do

    resources :bills          , only: [ :index ,:show, :create] do

 	     resources :bill_details   , only: [ :index ,:show]
    end

  end
  resources :pharmacies , only: [ :index ,:show ] do 


   resources :categories      , only: [ :index ,:show] do 

    resources :products       , only: [ :index ,:show] 
  	 
     end
    
    end

    resources :cities     , only: [:index,:show,:create,:update,:destroy]
    resources :fundations , only: [:index,:show,:create,:update,:destroy]
    resources :donations  , only: [:index,:show,:create,:update,:destroy]

 
 get  "/products/:id/pharmacies",                       to: "product_has_pharmacies#index"
 get  "/pharmacies/:id/products",                       to: "product_has_pharmacies#products_by_pharmacy"
 get  "/products",                                      to: "products#all_products"
 post "/access",                                        to: "users#access"
 get  "/mydonations",                                   to: "donations#user_donations"
end
